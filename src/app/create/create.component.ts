import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})

export class CreateComponent implements OnInit {

  checked = false;
  disabled = false;

  public imagePath;
  public message: string;
  imgURL: any;
  iconURL: any;
  showDiv = false;
  andares = [
    { value: 'andar-1', viewValue: '1º Andar' },
    { value: 'andar-2', viewValue: '2º Andar' },
    { value: 'andar-3', viewValue: '3º Andar' },
    { value: 'andar-4', viewValue: '4º Andar' },
    { value: 'andar-5', viewValue: 'Mezanino' },
  ];

  previewImagem(files) {
    if (files.length === 0)
      return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }

    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
  }
  previewIcon(files) {
    if (files.length === 0)
      return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }

    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.iconURL = reader.result;
    }
  }

  constructor() { }

  ngOnInit() {
  }

}
