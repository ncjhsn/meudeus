export interface Experimento {
    id: number
    titulo: string,
    categoria: string,
    principal: boolean,
    ativo: boolean
}