import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { createComponent } from '@angular/compiler/src/core';
import { CreateComponent } from './create/create.component';

const routes: Routes = [
  {path: 'admin', component: AdminComponent},
  {path: 'admin/criar', component: CreateComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
