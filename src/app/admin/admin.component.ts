import { Component, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
<<<<<<< HEAD
import {MatDialog} from '@angular/material/dialog';

export interface Experimento {
  titulo: string,
  categoria: string,
  principal: boolean,
  ativo: boolean
}
=======
import { Experimento } from '../entidades/experimento';
import { ApiService } from '../api.service';
import { HttpClientModule } from '@angular/common/http';
import { Subscription } from 'rxjs';
>>>>>>> 10250de1d2065c45b48b46d0656e23b0a85df0f2

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  selecionados = [];
  disabled: boolean = false;
  teste: boolean = false;
  indexData: Experimento[] = [
    { titulo: 'Experimento nº 1', categoria: 'Física', principal: false, ativo: true },
    { titulo: 'Experimento nº 2', categoria: 'Matemática', principal: true, ativo: true },
    { titulo: 'Experimento nº 3', categoria: 'Quimíca', principal: false, ativo: false },
    { titulo: 'Experimento nº 4', categoria: 'Quimíca', principal: true, ativo: false },
    { titulo: 'Experimento nº 5', categoria: 'Matemática', principal: false, ativo: true },
    { titulo: 'Experimento nº 6', categoria: 'Matemática', principal: true, ativo: true },
    { titulo: 'Experimento nº 7', categoria: 'Física', principal: false, ativo: true },
    { titulo: 'Experimento nº 8', categoria: 'Física', principal: true, ativo: true },
    { titulo: 'Experimento nº 9', categoria: 'Química', principal: false, ativo: true },
    { titulo: 'Experimento nº 10', categoria: 'Matemática', principal: true, ativo: true }
  ]

  colunas: string[] = ['excluir', 'titulo', 'categoria', 'principal', 'ativo', 'editar'];
  ordenador: Experimento[] = [];
  dataSource = new MatTableDataSource(this.indexData);


  @ViewChild(MatSort, { static: true }) sort: MatSort;


  isChecked(obj: any) {
    const indice = this.selecionados.indexOf(obj);
    if (indice === -1) {
      this.selecionados.push(obj);
    } else {
      this.selecionados.splice(indice, 1);
    }
    console.log(this.selecionados);
  }

  removeItem(selecionados) {
    const item = this.indexData = this.indexData.filter(obj => !this.selecionados.includes(obj));
    // AQUIII
    //this.selecionados[1] = 0;
    //this.selecionados;
  }

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, public dialog: MatDialog) {
    iconRegistry.addSvgIcon('search', sanitizer.bypassSecurityTrustResourceUrl('./assets/search.svg'));

  }

  filtrar(text: string) {
    this.dataSource.filter = text.trim().toLowerCase();
    console.log("maluco");
  }

  ngOnInit() {
    this.sub = this.api.listarExperimentos().subscribe((result) => { this.indexData = result });
    this.dataSource.sort = this.sort;
    // this.experimentos();
  }

  openDialog() {
    const dialogRef = this.dialog.open(AdminComponentConfirmComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result == true) {
        this.removeItem(this.selecionados);
        window.location.reload(false);
      } else {

      }
    });
  }

}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  console.log('benga')
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}

//------------------

@Component({
  selector: 'admin.component.confirm',
  templateUrl: 'admin.component.confirm.html',
})
export class AdminComponentConfirmComponent {}
