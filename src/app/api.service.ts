import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Experimento } from './entidades/experimento';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  PORT = 8080;
  urlTodos = "10.96.127.81:3000/experimento/buscaTodos";

  constructor(private http: HttpClient) { }

  listarExperimentos() {
    return this.http.get<Experimento[]>(this.urlTodos);
  }

  // criarExperimento() {
  //   const httpOptions= {headers: new HttpHeaders({ 'Content-Type': 'application/json' })};
  //   return this.http.post(this.urlCadCpxs, cpx, httpOptions );
  // }


}
